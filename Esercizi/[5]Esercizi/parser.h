#ifndef _PARSER_H_
#define _PARSER_H_

#include <stdio.h>
#include <string.h>

char currentCommand[50];


typedef enum enumCommandType{
    A,
    C,
    L,
} commandType;
enum enumCommandType type;
enum bool{
    false = 0,
    true = 1,
};

FILE *file;
void Initializer(FILE *f);
enum bool HasMoreCommands();
void Advance();
enum enumCommandType CommandType();
void Symbol(char out[]);
void Dest(char out[]);
void Comp(char out[]);
void Jump(char out[]);

#endif
