#include <stdio.h>
#include "parser.h"
#include "code.h"
#include "write.h"

int main(int argc, char const *argv[])
{

    Initializer(fopen("Add.asm", "r"));
    write_Initialize(fopen("out.hack", "w"));

    //while(HasMoreCommands())
    {
        Advance();
        enum enumCommandType type = CommandType();
        char word[16];
        char tmp[16];
        switch (type)
        {
            case A:
                w_write("0");
                Symbol(tmp);
                c_Binarize(tmp, word);
                w_write(word);
                break;
            case C:
                w_write("111");
                Comp(tmp);
                c_Comp(tmp, word);
                w_write(word);
                Dest(tmp);
                c_Dest(tmp, word);
                w_write(word);
                Jump(tmp);
                c_Jump(tmp,word);
                w_write(word);
            default:
                break;
        }
        w_write_escape();

    }    

    // Initializer(fopen("Add.asm", "r"));
    // char out[50];

    // while(HasMoreCommands()){
    //     Advance();
    //     enum enumCommandType type = CommandType();
    //     printf("%i \n", type);
    // }   
    // bit bits[10];
    // char test[] = "JGT";
    // c_Jump(test, bits);
    // printf("%s", bits);
    
    


    return 0;
}
