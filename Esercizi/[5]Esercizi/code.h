#ifndef _CODE_H_
#define _CODE_H_

#include <string.h>
#include <stdio.h>


typedef char bit;


void c_Dest(char dest[], bit out[]);
void c_Comp(char comp[], bit out[]);
void c_Jump(char jump[], bit out[]);
void c_Binarize(char integer[], bit out[]);
#endif