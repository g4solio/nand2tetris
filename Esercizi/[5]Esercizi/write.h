#ifndef _WRITE_H_
#define _WRITE_H_

#include <string.h>
#include <stdio.h>


FILE *file_write;

void write_Initialize(FILE *f);
void w_write(char bits[]);
void w_write_escape();

#endif